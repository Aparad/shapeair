/*
 * ShapeAir
 * Author: Radosław Matuszak
 * Creation date: 09.07.2018
 */


// Include neccesary libraries.
#include <Servo.h>
#include <Firmata.h>

// Program variables declaration.
Servo servo;
int current_servo_pos = 0;
// Callback function for message of type ANALOG_MESSAGE.
void analogWriteCallback(byte pin, int value)
{
  int converted_value = (int)(((float)value/255.0) * 180);
  if (converted_value < 180 && converted_value != current_servo_pos)  //If value is lower than maximum acceptable value...
  { 
    servo.write(converted_value); // ...change position.
    delay(60);
  }
}

/*  Initialization function
 *  1. Firmata protocol initialization:
 *   - sets Firmware version
 *   - binds callback function with message of type ANALOG_MESSAGE
 *   - sets communication baud rate at 9600 bits per second
 *  2. Servo initalization:
 *   - attaches Servo at 9th digital pin
 */
void setup()
{
  Firmata.setFirmwareVersion(FIRMATA_MAJOR_VERSION, FIRMATA_MINOR_VERSION);
  Firmata.attach(ANALOG_MESSAGE, analogWriteCallback);
  Firmata.begin(9600);
  servo.attach(9, 800, 2200);  
}

/*  Main program function.
 *  First, all incoming messages that are waiting in the buffer are processed.
 *  Next, all board analog inputs are sent to the computer.
 */
void loop()
{
  while (Firmata.available()) {
    Firmata.processInput();
  }
  for (byte analogPin = 0; analogPin < TOTAL_ANALOG_PINS; analogPin++) {
    Firmata.sendAnalog(analogPin, analogRead(analogPin));
    delay(1); //Delay controlling messages amount.
  }
}

