import socket
import time


class BlenderClient:
    """Socket client for delivering data about mesh from Blender"""

    def __init__(self):
        self.HOST = 'localhost'
        self.PORT = 8008
        self.BLENDER_SOCKET = socket.socket()
        self.BLENDER_CONNECTION = None
        self.BLENDER_ADDRESS = None
        self.startConnection()

    def startConnection(self):
        self.BLENDER_SOCKET.connect((self.HOST, self.PORT))

    def sendData(self):
        counter = 0
        while True:
            self.BLENDER_SOCKET.send(str(counter).encode())
            self.BLENDER_SOCKET.recv(1024)
            counter += 1
            time.sleep(1)


if __name__ == '__main__':
    bc = BlenderClient()
    bc.sendData()
    bc.BLENDER_SOCKET.close()