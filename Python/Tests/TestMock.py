#from unittest.mock import MagicMock


class TestMock:
    """ Board mock for testing/developing purposes. """
    def __init__(self):
        # Constant of real arm length
        self.ARM_LENGTH = 10
        self.MAX_HEIGHT = 20
        self.Y_MAX = 0.98
        self.Y_MIN = 0.1
        self.X_MAX = 0.9
        self.X_MIN = 0.1
        self.Z_MAX = 1
        self.Z_MIN = 0
        self.ANGLE_MAX = 180
        self.ANGLE_MIN = 0

    def setServoPosition(self, position):
        print("Servo position set to: %s" % str(position))

    def getKeyNumber(self):
        return -1

    def getX(self):
        return 0.3

    def getY(self):
        return 0.3

    def getZ(self):
        return 0.3