from ShapeAir.ShapeAirModule import ShapeAirModule, cancellable
from Utils.Logger import getLogger
import time


class testThread(ShapeAirModule):
    def __init__(self):
        super(testThread, self).__init__()
        self.addWorker(self.worker)
        self.counter = 1
        self.logger = getLogger()

    @cancellable
    def worker(self):
        self.logger.info('Test thread execution number: %d' % self.counter)
        self.counter += 1
        if self.counter > 5:
            self.counter = 1
            raise Exception
        time.sleep(1)
