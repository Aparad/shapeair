from ShapeAir.ShapeAirLogic import ShapeAirLogic
from UI.AppWindow import GUI
from Config.Configuration import Configuration
import sys


class ShapeAir:
    """ Startup class of the ShapeAir.
    Author: Radosław Matuszak
    Date: 09.07.2018r
    """
    def __init__(self):
        self.Config = Configuration().read()
        try:
            self.ShapeAirLogic = ShapeAirLogic()
        except Exception as e:
            print("An error occurred on startup.\nVerify that the Board is connected before script starts.")
            raise e


if __name__ == "__main__":
    ShapeAir = ShapeAir()
    if ShapeAir.Config.getboolean("conf", "guienabled"):
        """ Start with GUI. """
        GUI(ShapeAir)
    else:
        """ Start in console only mode. """
        ShapeAir.ShapeAirLogic.start()
    ShapeAir.ShapeAirLogic.stop()
    sys.exit()
