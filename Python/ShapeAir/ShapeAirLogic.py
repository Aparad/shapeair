from Utils.WinNotification import WinNotification
from Utils.Singleton import Singleton
from Controllers.KeyboardController import KeyboardController
from Controllers.MouseController import MouseController
from Blender.BlenderServer import BlenderServer
from Controllers.ServoController import ServoController
from Utils.Logger import Logger
from Config.Configuration import Configuration
from ShapeAir.ShapeAirThread import ShapeAirThread, getStopThreadsEvent
from Connectors.BoardConnector import BoardConnector
from time import sleep
from UI.UIUpdater import UIUpdater
from Tests.TestThread import testThread


class ShapeAirLogic(metaclass=Singleton):
    """ Main tasks of the Application.
        1. Convert data read from board into mouse movements, clicks and keypresses.
        2. Forward data from Blender to the board.

        Those tasks are prepared to run in parallel threads. """

    # Current ShapeAir status: False - not working; True - working.
    IS_WORKING = False
    THREADS = []

    def __init__(self, _customConfig = None):
        # ShapeAir Interface Variables #
        # Configuration object
        self.Config = Configuration().read() if _customConfig is None else _customConfig
        # Messages logger
        self._Logger = Logger(self.Config.get("conf", "logging"), "shapeAirLogger")
        self.Logger = self._Logger.Logger
        # Mouse controller object
        self.Mouse = MouseController()
        # Blender Server object
        self.BlenderServer = BlenderServer(self.Config.getint("conf", "blenderserverport"))
        # DC Engine Controller object
        self.Servo = ServoController()
        # Keyboard controller object
        self.Keyboard = KeyboardController()
        # De/Activate Win 10 notifications according to configuration file
        WinNotification.NOTIFICATIONS_ACTIVE = self.Config.getboolean("conf", "winnotifications")

        self.modules = [
            {
                "module": self.Keyboard,
                "threadName": "Arduino Keyboard Actions",
                "isOn": self.Config.getboolean("conf", "keyboardenabled")
            },
            {
                "module": self.Mouse,
                "threadName": "Mouse X/Y movement simulation",
                "isOn": self.Config.getboolean("conf", "mousecontrol")
            },
            {
                "module": self.BlenderServer,
                "threadName": "Blender Server",
                "isOn": self.Config.getboolean("conf", "forcefeedback")
            },
            {
                "module": self.Servo,
                "threadName": "Servo Motor manipulation",
                "isOn": self.Config.getboolean("conf", "forcefeedback")
            },
            {
                "module": testThread(),
                "threadName": "Test thread",
                "isOn": False
            }
        ]

    def initializeThreads(self):
        for module in self.modules:
            if module["isOn"]:
                for worker in module["module"].workers:
                    sat = ShapeAirThread(target=self.prepareWorker(worker), args=module["threadName"])
                    for initWorker in module["module"].initWorkers:
                        sat.initWorkers.append(self.prepareWorker(initWorker))
                    for finWorker in module["module"].finWorkers:
                        sat.finWorkers.append(self.prepareWorker(finWorker))
                    ShapeAirLogic.THREADS.append(sat)
        return self

    def start(self):
        getStopThreadsEvent().clear()
        self.initializeThreads()
        for thread in ShapeAirLogic.THREADS:
            for initWorker in thread.initWorkers:
                initWorker()
            thread.start()
        ShapeAirLogic.IS_WORKING = True

    @classmethod
    def stop(cls):
        getStopThreadsEvent().set()
        for thread in ShapeAirLogic.THREADS:
            thread.join()
            for finWorker in thread.finWorkers:
                finWorker()
        ShapeAirLogic.clearState()

    @classmethod
    def clearState(cls):
        ShapeAirLogic.THREADS.clear()
        ShapeAirLogic.IS_WORKING = False

    def exit(self):
        if ShapeAirLogic.IS_WORKING:
            self.stop()
            sleep(1.5)
        if self.Config.getboolean("conf", "boardenabled"):
            BoardConnector().disconnect()

    def prepareWorker(self, worker):
        def _prepareWorker():
            try:
                worker()
            except Exception:
                getStopThreadsEvent().set()
                ShapeAirLogic.clearState()
                UIUpdater.updateGUI()
        return _prepareWorker
