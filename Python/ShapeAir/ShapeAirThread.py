from threading import Thread, Event, Lock, RLock


class ShapeAirThread(Thread):
    __stopThreadsEvent = Event()
    __threadsGlobalLock = Lock()
    __threadsGlobalRLock = RLock()

    def __init__(self,group=None, target=None, args=(), kwargs=None):
        super(ShapeAirThread, self).__init__(group=group, target=target)
        self.args = args
        self.kwargs = kwargs
        self.daemon = True
        self.initWorkers = []
        self.finWorkers = []


def getLock():
    return ShapeAirThread._ShapeAirThread__threadsGlobalLock


def getRLock():
    return ShapeAirThread._ShapeAirThread__threadsGlobalRLock


def getStopThreadsEvent():
    return ShapeAirThread._ShapeAirThread__stopThreadsEvent
