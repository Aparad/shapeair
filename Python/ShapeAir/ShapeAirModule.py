from functools import wraps
from ShapeAir.ShapeAirThread import getStopThreadsEvent


class ShapeAirModule:
    """ Application module template.
        Holds refs to callables of modules' tasks. """
    def __init__(self):
        super(ShapeAirModule, self).__init__()

        # Module initialize workers
        self.initWorkers = []

        # Module workers
        self.workers = []

        # Module finalize workers
        self.finWorkers = []

    """ Workers are methods defined by controllers.
        Order of adding workers matters especially for initializing and finalizing workers,
        because it determines order of execution during thread initialization/finalization. """

    def addInitWorker(self, workerFunc):
        self.initWorkers.append(workerFunc)

    def deleteInitWorker(self, workerFunc):
        self.initWorkers.remove(workerFunc)

    def addWorker(self, workerFunc):
        self.workers.append(workerFunc)

    def deleteWorker(self, workerFunc):
        self.workers.remove(workerFunc)

    def addFinWorker(self, workerFunc):
        self.finWorkers.append(workerFunc)

    def deleteFinWorker(self, workerFunc):
        self.finWorkers.remove(workerFunc)


def cancellable(worker):
    @wraps(worker)
    def _cancellable(*args, **kwargs):
        while not getStopThreadsEvent().isSet():
            worker(*args, **kwargs)
    return _cancellable


