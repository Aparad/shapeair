from win10toast import ToastNotifier


class WinNotification:
    """ Win 10 popup notifications. """
    NOTIFICATIONS_ACTIVE = True

    def __init__(self):
        # Win 10 notifications
        self.Win10Notification = ToastNotifier()

    def showWinNotification(self, title, message):
        if WinNotification.NOTIFICATIONS_ACTIVE:
            self.Win10Notification.show_toast(title, message, icon_path=None, duration=3, threaded=True)