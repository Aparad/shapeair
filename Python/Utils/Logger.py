import logging
from Utils.Singleton import Singleton


class Logger(metaclass=Singleton):
    """ Application messages logger.
        Possible logging thresholds: debug, info, warning, error, critical. """

    _loggingLevels = {
        "debug": logging.DEBUG,
        "info": logging.INFO,
        "warning": logging.WARNING,
        "error": logging.ERROR,
        "critical": logging.CRITICAL
    }

    NAME = None

    def __init__(self, level, name):
        Logger.NAME = name
        self.Level = level

        self.Formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')

        self.Handler = logging.StreamHandler()
        self.Handler.setFormatter(self.Formatter)

        self.Logger = logging.getLogger(Logger.NAME)
        self.Logger.setLevel(Logger._loggingLevels[self.Level])
        self.Logger.addHandler(self.Handler)


def getLogger():
    return logging.getLogger(Logger.NAME)
