import configparser
from configparser import NoSectionError
from Utils.Singleton import Singleton


class Configuration(metaclass=Singleton):
    """ Program configuration class. """
    CONFIG = None

    def __init__(self, path="./Config/conf.ini"):
        Configuration.CONFIG = configparser.ConfigParser()
        if len(Configuration.CONFIG.read(path)) == 0:
            raise Exception("Could not read Config/conf.ini. Make sure it's there and has [conf] section.")

    @classmethod
    def read(cls):
        return Configuration.CONFIG
