import socket
from Utils.Singleton import Singleton
from ShapeAir.ShapeAirModule import ShapeAirModule, cancellable
from ShapeAir.ShapeAirThread import getRLock
from Utils.Logger import getLogger
from Config.Configuration import Configuration
from time import sleep


class BlenderServer(ShapeAirModule, metaclass=Singleton):
    DATA_CORRECT = 1
    DATA_EMPTY = 2
    DATA_EMPTY_INVALID = 0
    RECEIVED_DATA = 0
    DATA_STATUS = None
    """Socket server listening for blender data"""
    def __init__(self, port):
        super(BlenderServer, self).__init__()
        self.Port = port
        self.Host = 'localhost'
        self.BenderSocket = None
        self.BlenderConnection = None
        self.BlenderAddress = None
        self.ServerLock = getRLock()

        self.Config = Configuration().read()
        self.Logger = getLogger()

        self.addInitWorker(self.startConnection)
        self.addWorker(self.run)
        self.addFinWorker(self.closeConnection)

    def startConnection(self, socket_timeout=1):
        self.Logger.info("Start ShapeAir in Blender now...")
        try:
            self.BenderSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.BenderSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.BenderSocket.bind((self.Host, self.Port))
        except OSError:
            self.Logger.debug("Connecting failed. Socket is probably already bound.")
            return False
        try:
            self.BenderSocket.settimeout(socket_timeout)
            self.BenderSocket.listen(1)
            self.BlenderConnection, self.BlenderAddress = self.BenderSocket.accept()
            self.BlenderConnection.settimeout(0.2)
            BlenderServer.DATA_STATUS = BlenderServer.DATA_EMPTY
            self.Logger.info("Connecting with Blender %s successful!" % str(self.BlenderAddress))
        except socket.timeout:
            self.Logger.error("Connecting failed. ShapeAir add-on in Blender probably didn't try to connect.")
            return False
        return True

    def fetchData(self):
        """ Receives data from Blender and saves it as RECEIVED_DATA, also sets variable DATA_STATUS accordingly to the data content.
            If data is available and valid, then DATA_STATUS is set to DATA_CORRECT.
            If no data was received during connection timeout, DATA_STATUS is set to DATA_EMPTY.
            If the connection was lost or data is invalid, DATA_STATUS is set to DATA_EMPTY_INVALID.
            """
        with self.ServerLock:
            try:
                BlenderServer.RECEIVED_DATA = self.BlenderConnection.recv(1024).decode('utf-8')
                if BlenderServer.RECEIVED_DATA:
                    self.Logger.debug("Received from blender:" + BlenderServer.RECEIVED_DATA)
                    BlenderServer.DATA_STATUS = BlenderServer.DATA_CORRECT
                    return True
                else:
                    self.Logger.error("RECEIVED DATA IS INVALID: %s" % self.RECEIVED_DATA)
                    BlenderServer.DATA_STATUS = BlenderServer.DATA_EMPTY_INVALID
                    return False
            except socket.timeout as e:
                self.Logger.error(str(e))
                BlenderServer.DATA_STATUS = BlenderServer.DATA_EMPTY
                return True
            except Exception as e:
                self.Logger.error("Error occurred during receiving data from Blender.\n%s" % str(e))
                BlenderServer.DATA_STATUS = BlenderServer.DATA_EMPTY_INVALID
                return False

    def closeConnection(self):
        try:
            self.BlenderConnection.shutdown(socket.SHUT_RDWR)
            self.BlenderConnection.close()
            self.Logger.info("Closed connection with Blender successfully.")
        except Exception as e:
            self.Logger.error("Error occurred during closing connection with Blender. Probably wasn't connected at all.\n%s" % str(e))
            return False
        finally:
            try:
                self.BenderSocket.close()
                return True
            except Exception as e:
                self.Logger.error("Error occurred during closing socket.\n%s" % str(e))
                return False

    @cancellable
    def run(self):
        self.Logger.debug("Reading Blender data...")
        with self.ServerLock:
            self.fetchData()
        if BlenderServer.DATA_STATUS == BlenderServer.DATA_EMPTY_INVALID:
            """ Try to reconnect if something went wrong."""
            self.Logger.error("Received data was invalid. Reconnecting with Blender now...")
            self.closeConnection()
            self.startConnection()
        sleep(0.1)
