# -*- coding: UTF-8 -*-
import bpy
from bpy_extras import view3d_utils
import socket

HOST = 'localhost'
PORT = 8008
BLENDER_SOCKET = None
BLENDER_CONNECTION = None
BLENDER_ADDRESS = None
REFRESH_RATE = 0.25  # Data sending interval in seconds


def calculateNormalToViewAlignment(context, event):
    """Run this function on left mouse, execute the ray cast"""
    # get the context arguments
    scene = context.scene
    region = context.region
    rv3d = context.region_data
    coord = event.mouse_region_x, event.mouse_region_y

    # get the ray from the viewport and mouse
    view_vector = view3d_utils.region_2d_to_vector_3d(region, rv3d, coord)
    ray_origin = view3d_utils.region_2d_to_origin_3d(region, rv3d, coord)

    ray_target = ray_origin + view_vector

    def visible_objects_and_duplis():
        """Loop over (object, matrix) pairs (mesh only)"""

        for obj in context.visible_objects:
            if obj.type == 'MESH':
                yield (obj, obj.matrix_world.copy())

            if obj.dupli_type != 'NONE':
                obj.dupli_list_create(scene)
                for dob in obj.dupli_list:
                    obj_dupli = dob.object
                    if obj_dupli.type == 'MESH':
                        yield (obj_dupli, dob.matrix.copy())

            obj.dupli_list_clear()

    def obj_ray_cast(obj, matrix):
        """Wrapper for ray casting that moves the ray into object space"""

        # get the ray relative to the object
        matrix_inv = matrix.inverted()
        ray_origin_obj = matrix_inv * ray_origin
        ray_target_obj = matrix_inv * ray_target
        ray_direction_obj = ray_target_obj - ray_origin_obj

        # cast the ray
        success, location, normal, face_index = obj.ray_cast(ray_origin_obj, ray_direction_obj)

        if success:
            return location, normal, face_index
        else:
            return None, None, None

    # cast rays and find the closest object
    best_length_squared = -1.0
    best_obj = None

    for obj, matrix in visible_objects_and_duplis():
        if obj.type == 'MESH':
            hit, normal, face_index = obj_ray_cast(obj, matrix)
            if hit is not None:
                hit_world = matrix * hit
                scene.cursor_location = hit_world
                length_squared = (hit_world - ray_origin).length_squared
                if best_obj is None or length_squared < best_length_squared:
                    best_length_squared = length_squared
                    best_obj = normal
    print("N=%s View vector=%s Dot product=%s" % (normal, view_vector, normal.dot(view_vector)))
    return best_obj.dot(view_vector)


    # now we have the object under the mouse cursor,
    # we could do lots of stuff but for the example just select.
    #if best_obj is not None:
        #best_obj.select = True
        #context.scene.objects.active = best_obj

class ShapeAirButton(bpy.types.Panel):
    bl_idname = "panel.ShapeAirButton"
    bl_label = "ShapeAir"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"

    def draw(self, context):
        self.layout.operator("object.shape_air_connection_operator", icon='MESH_CUBE', text="ShapeAir")


timer = None


class ShapeAirConnectionOperator(bpy.types.Operator):
    """ Starts connection with ShapeAir interface application.\n
    Press this button while the application is ready for connection."""
    bl_idname = "object.shape_air_connection_operator"
    bl_label = "ShapeAir Connection Operator"
    SHAPEAIR_ENABLED = False


    @classmethod
    def poll(cls, context):
        return context.active_object is not None

    def execute(self, context):
        if not ShapeAirConnectionOperator.SHAPEAIR_ENABLED:

            try:
                global BLENDER_SOCKET, BLENDER_ADDRESS, BLENDER_CONNECTION
                BLENDER_SOCKET = socket.socket()
                BLENDER_SOCKET.connect((HOST, PORT))
                print(BLENDER_SOCKET.getsockopt(socket.SOL_SOCKET, socket.SO_ERROR))
                print("Connection with ShapeAir server established.")
                try:
                    global timer
                    timer = wm.event_timer_add(REFRESH_RATE, bpy.context.window)
                    ShapeAirConnectionOperator.SHAPEAIR_ENABLED = True
                except Exception as e:
                    bpy.context.window_manager.event_timer_remove(timer)
            except Exception as e:
                print("Could not connect with ShapeAir server. Make sure it's listening.\nError:%s\n" % str(e))
                ShapeAirConnectionOperator.SHAPEAIR_ENABLED = False
        else:
            try:
                BLENDER_SOCKET.shutdown(socket.SHUT_RDWR)
                BLENDER_SOCKET.close()
                print("Connection with ShapeAir server closed.")
            except Exception as e:
                print("Could not close connection with ShapeAir server. Maybe it was already closed.\nError:%s\n" % str(e))
                ShapeAirConnectionOperator.SHAPEAIR_ENABLED = False
            finally:
                try:
                    bpy.context.window_manager.event_timer_remove(timer)
                except Exception as e:
                    print("Could not remove Blender Timer. If reconnectiong doesn't help, Blender restart is recommended.\nError:%s\n" % str(e))
                finally:
                    ShapeAirConnectionOperator.SHAPEAIR_ENABLED = False

        return {'FINISHED'}


class ViewOperatorRayCast(bpy.types.Operator):
    """ Ray cast into 3D viewport and return hit polygon alignment with the observer."""
    bl_idname = "view3d.modal_operator_raycast"
    bl_label = "RayCast View Operator"
    bl_info = {"name": "ShapeAir", "category": "All"}

    @classmethod
    def poll(cls, context):
        return context.space_data.type == 'VIEW_3D'

    def invoke(self, context, event):
        dot_product = calculateNormalToViewAlignment(context, event)
        try:
            message = str(dot_product).encode()
            if message != b'None':
                BLENDER_SOCKET.send(message)
        except Exception as e:
            print("Error occurred while sending data to ShapeAir server. Trying to close connection.\nError:%s\n" % str(e))
            global timer
            bpy.context.window_manager.event_timer_remove(timer)
            ShapeAirConnectionOperator.SHAPEAIR_ENABLED = False
        return {'RUNNING_MODAL'}


def register():
    bpy.utils.register_class(ShapeAirConnectionOperator)


def unregister():
    bpy.utils.unregister_class(ShapeAirConnectionOperator)


if __name__ == "__main__":
    bpy.utils.register_class(ShapeAirButton)
    bpy.utils.register_class(ShapeAirConnectionOperator)
    bpy.utils.register_class(ViewOperatorRayCast)
    wm = bpy.context.window_manager
    kc = wm.keyconfigs['Blender User']
    km = kc.keymaps['3D View']
    kmi = km.keymap_items.new(ViewOperatorRayCast.bl_idname, 'TIMER', 'ANY', ctrl=False)
