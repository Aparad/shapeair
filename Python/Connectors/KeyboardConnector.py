from Interfaces.BoardInterfaces import KeyboardInterface
from Connectors.BoardConnector import BoardConnector
from time import sleep


class KeyboardConnector(KeyboardInterface):
    """ Class providing interactions with the Arduino Keyboard for keystrokes simulations purposes. """
    def __init__(self):
        super(KeyboardConnector, self).__init__()

        self.Board = BoardConnector()

        # Initializing input data sources
        self.KeyboardPin = self.Board.registerPin('a', 3, 'i')

        # Array of voltage ranges for keyboard input interpretation
        self.ADCKeyValue = (600, 650, 700, 800, 900)

        # Wait until coming data is valid
        self.__readDataUntilValid()

    def __readDataUntilValid(self):
        """ Wait until data coming from Arduino is valid.
            First few chunks of data after connecting are useless. """
        while isinstance(self.KeyboardPin.read(), (type(None))):
            sleep(0.25)

    def getKeyNumber(self):
        """ Number of currently pressed key on Arduino keyboard.
            -1 : no button is pressed.
        """
        _KEYPRESS_VAL = self.KeyboardPin.read()
        for KEY_NUMBER in range(len(self.ADCKeyValue)):
            if _KEYPRESS_VAL * 1000 < self.ADCKeyValue[KEY_NUMBER]:
                return KEY_NUMBER
        return -1
