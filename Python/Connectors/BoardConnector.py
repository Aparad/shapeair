from pyfirmata import Arduino, util
from Utils.Singleton import Singleton
from Utils.Logger import getLogger


class BoardConnector(metaclass=Singleton):
    """ Class providing methods for reading from and writing to the board.
        This implementation is based on the Firmata protocol for serial communication with Arduino Uno board."""
    def __init__(self):
        super(BoardConnector, self).__init__()
        # Messages logger
        self.Logger = getLogger()
        # Arduino board object. Set on port COM3 and default baud rate 9600
        try:
            self.Board = Arduino('\.\COM3', baudrate=9600)
        except IOError as e:
            self.Logger.error("Couldn't connect to the board.")
            self.Logger.error(str(e))
            raise e
        # Iterator prevents Serial port from overflowing with data
        self.Iterator = util.Iterator(self.Board)
        self.Iterator.start()
        # Dictionary of currently used pins
        self.RegisteredPins = {}

    def registerPin(self, pinType, pinNumber, pinMode):
        if '%s%d' % (pinType, pinNumber) not in self.RegisteredPins.keys():
            self.RegisteredPins['%s%d' % (pinType, pinNumber)] = self.Board.get_pin('%s:%d:%s' % (pinType, pinNumber, pinMode))
            return self.RegisteredPins['%s%d' % (pinType, pinNumber)]
        else:
            self.Logger.error("Pin {pin} already taken!".format(pin='%s%d' % (pinType, pinNumber)))

    def unregisterPin(self, pinType, pinNumber):
        if '%s%d' % (pinType, pinNumber) in self.RegisteredPins.keys():
            self.RegisteredPins.pop('%s%d' % (pinType, pinNumber))
        else:
            self.Logger.warn("Pin {pin} was not taken!".format(pin='%s%d' % (pinType, pinNumber)))

    def disconnect(self):
        self.Board.exit()
