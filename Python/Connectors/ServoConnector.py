from Interfaces.BoardInterfaces import ServoInterface
from Connectors.BoardConnector import BoardConnector


class ServoConnector(ServoInterface):
    """ Class providing methods for interaction with Arduino Servo Motor. """
    def __init__(self):
        super(ServoConnector, self).__init__()

        self.Board = BoardConnector()

        # Initializing output data sources
        self.ServoPin = self.Board.registerPin('d', 9, 'p')

    def setServoPosition(self, speed):
        try:
            return self.ServoPin.write(speed)
        # TODO: Specify exception
        except Exception as e:
            print(e)
