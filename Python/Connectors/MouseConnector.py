from Interfaces.BoardInterfaces import MouseInterface
from Connectors.BoardConnector import BoardConnector
from time import sleep


class MouseConnector(MouseInterface):
    """ Class providing interactions with the Arduino Board for mouse simulation purposes. """
    def __init__(self):
        super(MouseConnector, self).__init__()

        self.Board = BoardConnector()

        # Initializing input data sources
        self.PinX = self.Board.registerPin('a', 0, 'i')
        self.PinY = self.Board.registerPin('a', 1, 'i')
        self.PinZ = self.Board.registerPin('a', 2, 'i')

        # Wait until coming data is valid
        self.__readDataUntilValid()

    def __readDataUntilValid(self):
        """ Wait until data coming from Arduino is valid.
            First few chunks of data after connecting are useless. """
        while (not all(not isinstance(dataTest, (type(None))) for dataTest in (
                self.PinX.read(),
                self.PinY.read(),
                self.PinZ.read()
        ))):
            sleep(0.25)

    def getX(self):
        try:
            return self.PinX.read()
        except TypeError:
            print("Received invalid mouse position data")
            return 0

    def getY(self):
        try:
            return self.PinY.read()
        except TypeError:
            print("Received invalid mouse position data")
            return 0

    def getZ(self):
        try:
            return self.PinZ.read()
        except TypeError:
            print("Received invalid mouse position data")
            return 0
