class BlenderShapeAirInterface:
    def __init__(self):
        super(BlenderShapeAirInterface, self).__init__()

    def processInputFromBlenderIntoServoPosition(self, inputValue):
        raise NotImplementedError
