class MouseInterface:
    def __init__(self):
        super(MouseInterface, self).__init__()

    def getX(self):
        raise NotImplementedError

    def getY(self):
        raise NotImplementedError

    def getZ(self):
        raise NotImplementedError


class KeyboardInterface:
    def __init__(self):
        super(KeyboardInterface, self).__init__()

    def getKeyNumber(self):
        raise NotImplementedError


class ServoInterface:
    def __init__(self):
        super(ServoInterface, self).__init__()

    def setServoPosition(self, speed):
        raise NotImplementedError
