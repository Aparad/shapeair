/********************************************************************************
** Form generated from reading UI file 'shapeair.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef SHAPEAIRUI_H
#define SHAPEAIRUI_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSlider>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QDialogButtonBox *buttonsBox;
    QSlider *horizontalSlider;
    QLabel *label;
    QCheckBox *mouseControlCheckbox;
    QLabel *label_2;
    QCheckBox *checkBox_3;
    QSlider *foreFeedbackSlider;
    QLabel *label_3;
    QCheckBox *returnMouseSensitivityWhenDisabledCheckbox;
    QFrame *frame;
    QFrame *frame_2;
    QCheckBox *checkBox_4;
    QCheckBox *alwaysOnTopCheckbox;
    QFrame *line;

    void setupUi(QDialog *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(275, 264);
        MainWindow->setWindowOpacity(0.75);
        buttonsBox = new QDialogButtonBox(MainWindow);
        buttonsBox->setObjectName(QStringLiteral("buttonsBox"));
        buttonsBox->setGeometry(QRect(10, 230, 251, 32));
        buttonsBox->setLocale(QLocale(QLocale::English, QLocale::UnitedStates));
        buttonsBox->setOrientation(Qt::Horizontal);
        buttonsBox->setStandardButtons(QDialogButtonBox::Apply|QDialogButtonBox::Discard|QDialogButtonBox::RestoreDefaults);
        buttonsBox->setCenterButtons(true);
        horizontalSlider = new QSlider(MainWindow);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setGeometry(QRect(20, 190, 231, 22));
        horizontalSlider->setPageStep(1);
        horizontalSlider->setOrientation(Qt::Horizontal);
        horizontalSlider->setTickPosition(QSlider::NoTicks);
        label = new QLabel(MainWindow);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 170, 91, 16));
        mouseControlCheckbox = new QCheckBox(MainWindow);
        mouseControlCheckbox->setObjectName(QStringLiteral("mouseControlCheckbox"));
        mouseControlCheckbox->setGeometry(QRect(20, 130, 131, 17));
        mouseControlCheckbox->setChecked(true);
        label_2 = new QLabel(MainWindow);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 0, 71, 31));
        checkBox_3 = new QCheckBox(MainWindow);
        checkBox_3->setObjectName(QStringLiteral("checkBox_3"));
        checkBox_3->setGeometry(QRect(20, 40, 131, 17));
        checkBox_3->setChecked(true);
        foreFeedbackSlider = new QSlider(MainWindow);
        foreFeedbackSlider->setObjectName(QStringLiteral("foreFeedbackSlider"));
        foreFeedbackSlider->setGeometry(QRect(20, 80, 231, 22));
        foreFeedbackSlider->setMaximum(5);
        foreFeedbackSlider->setPageStep(1);
        foreFeedbackSlider->setOrientation(Qt::Horizontal);
        foreFeedbackSlider->setTickPosition(QSlider::TicksBelow);
        label_3 = new QLabel(MainWindow);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(20, 60, 131, 16));
        returnMouseSensitivityWhenDisabledCheckbox = new QCheckBox(MainWindow);
        returnMouseSensitivityWhenDisabledCheckbox->setObjectName(QStringLiteral("returnMouseSensitivityWhenDisabledCheckbox"));
        returnMouseSensitivityWhenDisabledCheckbox->setGeometry(QRect(20, 150, 191, 17));
        returnMouseSensitivityWhenDisabledCheckbox->setLayoutDirection(Qt::LeftToRight);
        returnMouseSensitivityWhenDisabledCheckbox->setAutoFillBackground(false);
        frame = new QFrame(MainWindow);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setGeometry(QRect(10, 30, 251, 81));
        frame->setFrameShape(QFrame::Box);
        frame->setFrameShadow(QFrame::Raised);
        frame->setLineWidth(1);
        frame->setMidLineWidth(1);
        frame_2 = new QFrame(MainWindow);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        frame_2->setGeometry(QRect(10, 120, 251, 101));
        frame_2->setFrameShape(QFrame::Box);
        frame_2->setFrameShadow(QFrame::Raised);
        frame_2->setLineWidth(1);
        frame_2->setMidLineWidth(1);
        checkBox_4 = new QCheckBox(MainWindow);
        checkBox_4->setObjectName(QStringLiteral("checkBox_4"));
        checkBox_4->setGeometry(QRect(100, 0, 70, 31));
        alwaysOnTopCheckbox = new QCheckBox(MainWindow);
        alwaysOnTopCheckbox->setObjectName(QStringLiteral("alwaysOnTopCheckbox"));
        alwaysOnTopCheckbox->setGeometry(QRect(170, 0, 91, 31));
        line = new QFrame(MainWindow);
        line->setObjectName(QStringLiteral("line"));
        line->setGeometry(QRect(76, 5, 20, 19));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        frame->raise();
        frame_2->raise();
        buttonsBox->raise();
        horizontalSlider->raise();
        label->raise();
        mouseControlCheckbox->raise();
        label_2->raise();
        checkBox_3->raise();
        foreFeedbackSlider->raise();
        label_3->raise();
        returnMouseSensitivityWhenDisabledCheckbox->raise();
        checkBox_4->raise();
        alwaysOnTopCheckbox->raise();
        line->raise();

        retranslateUi(MainWindow);
        QObject::connect(buttonsBox, SIGNAL(accepted()), MainWindow, SLOT(accept()));
        QObject::connect(buttonsBox, SIGNAL(rejected()), MainWindow, SLOT(reject()));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QDialog *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "ShapeAir", nullptr));
        label->setText(QApplication::translate("MainWindow", "Mouse sensitivity", nullptr));
        mouseControlCheckbox->setText(QApplication::translate("MainWindow", "Mouse control", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "Configuration", nullptr));
        checkBox_3->setText(QApplication::translate("MainWindow", "Force feedback", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "Force feedback  strength", nullptr));
        returnMouseSensitivityWhenDisabledCheckbox->setText(QApplication::translate("MainWindow", "Return to default when disabled", nullptr));
        checkBox_4->setText(QApplication::translate("MainWindow", "Opaque", nullptr));
        alwaysOnTopCheckbox->setText(QApplication::translate("MainWindow", "Always on top", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // SHAPEAIRUI_H
