from subprocess import call
call(
    [
        "python.exe",
        "-m",
        "PyQt5.uic.pyuic",
        "..\\..\\UI\\shapeair.ui",
        "-o",
        "..\\..\\UI\\ShapeAirUI.py"
    ],
    cwd="..\\venv\\Scripts"
)