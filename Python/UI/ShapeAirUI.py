# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '..\..\UI\shapeair.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):

    SHAPEAIR_ENABLED = False

    WINDOW_OPACITY = False
    WINDOW_OPACITY_LEVEL = 0.5

    ALWAYS_ON_TOP = False

    def __init__(self, shapeAir):
        super(Ui_MainWindow, self).__init__()
        self.shapeAir = shapeAir
        self.components = shapeAir.ShapeAirLogic

    def setupUi(self, MainWindow):
        self.MainWindow = MainWindow
        MainWindow.setObjectName("MainWindow")
        MainWindow.setEnabled(True)
        MainWindow.resize(275, 264)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(275, 244))
        MainWindow.setMaximumSize(QtCore.QSize(275, 244))
        MainWindow.setWindowOpacity(1.0)
        self.buttonsBox = QtWidgets.QDialogButtonBox(MainWindow)
        self.buttonsBox.setGeometry(QtCore.QRect(10, 210, 251, 32))
        self.buttonsBox.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.buttonsBox.setOrientation(QtCore.Qt.Horizontal)
        #self.buttonsBox.setStandardButtons(QtWidgets.QDialogButtonBox.Apply|QtWidgets.QDialogButtonBox.Discard|QtWidgets.QDialogButtonBox.RestoreDefaults)
        self.buttonsBox.setStandardButtons(QtWidgets.QDialogButtonBox.Discard)
        self.buttonsBox.button(QtWidgets.QDialogButtonBox.Discard).setText('Start')
        self.buttonsBox.setCenterButtons(True)
        self.buttonsBox.setObjectName("buttonsBox")
        self.mouseSensitivitySlider = QtWidgets.QSlider(MainWindow)
        self.mouseSensitivitySlider.setGeometry(QtCore.QRect(20, 170, 231, 22))
        self.mouseSensitivitySlider.setPageStep(1)
        self.mouseSensitivitySlider.setOrientation(QtCore.Qt.Horizontal)
        self.mouseSensitivitySlider.setTickPosition(QtWidgets.QSlider.NoTicks)
        self.mouseSensitivitySlider.setObjectName("horizontalSlider")
        self.mouseSensitivitySlider.setValue(99)
        self.mouseSensitivityLabel = QtWidgets.QLabel(MainWindow)
        self.mouseSensitivityLabel.setGeometry(QtCore.QRect(20, 150, 91, 16))
        self.mouseSensitivityLabel.setObjectName("label")
        self.mouseControlCheckbox = QtWidgets.QCheckBox(MainWindow)
        self.mouseControlCheckbox.setGeometry(QtCore.QRect(20, 130, 131, 17))
        self.mouseControlCheckbox.setChecked(False)
        self.mouseControlCheckbox.setObjectName("mouseControlCheckbox")
        self.configurationLabel = QtWidgets.QLabel(MainWindow)
        self.configurationLabel.setGeometry(QtCore.QRect(10, 0, 71, 31))
        self.configurationLabel.setObjectName("label_2")
        self.forceFeedbackCheckbox = QtWidgets.QCheckBox(MainWindow)
        self.forceFeedbackCheckbox.setGeometry(QtCore.QRect(20, 40, 131, 17))
        self.forceFeedbackCheckbox.setChecked(False)
        self.forceFeedbackCheckbox.setObjectName("checkBox_3")
        self.forceFeedbackSlider = QtWidgets.QSlider(MainWindow)
        self.forceFeedbackSlider.setGeometry(QtCore.QRect(20, 80, 231, 22))
        self.forceFeedbackSlider.setMaximum(5)
        self.forceFeedbackSlider.setPageStep(1)
        self.forceFeedbackSlider.setOrientation(QtCore.Qt.Horizontal)
        self.forceFeedbackSlider.setTickPosition(QtWidgets.QSlider.TicksBelow)
        self.forceFeedbackSlider.setObjectName("foreFeedbackSlider")
        self.forceFeedbackSlider.setValue(5)
        self.forceFeedbackLabel = QtWidgets.QLabel(MainWindow)
        self.forceFeedbackLabel.setGeometry(QtCore.QRect(20, 60, 131, 16))
        self.forceFeedbackLabel.setObjectName("label_3")
        # self.returnMouseSensitivityWhenDisabledCheckbox = QtWidgets.QCheckBox(MainWindow)
        # self.returnMouseSensitivityWhenDisabledCheckbox.setGeometry(QtCore.QRect(20, 150, 191, 17))
        # self.returnMouseSensitivityWhenDisabledCheckbox.setLayoutDirection(QtCore.Qt.LeftToRight)
        # self.returnMouseSensitivityWhenDisabledCheckbox.setAutoFillBackground(False)
        # self.returnMouseSensitivityWhenDisabledCheckbox.setObjectName("returnMouseSensitivityWhenDisabledCheckbox")
        self.frame = QtWidgets.QFrame(MainWindow)
        self.frame.setGeometry(QtCore.QRect(10, 30, 251, 81))
        self.frame.setFrameShape(QtWidgets.QFrame.Box)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setLineWidth(1)
        self.frame.setMidLineWidth(1)
        self.frame.setObjectName("frame")
        self.frame_2 = QtWidgets.QFrame(MainWindow)
        self.frame_2.setGeometry(QtCore.QRect(10, 120, 251, 81))
        self.frame_2.setFrameShape(QtWidgets.QFrame.Box)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setLineWidth(1)
        self.frame_2.setMidLineWidth(1)
        self.frame_2.setObjectName("frame_2")
        self.opaqueCheckbox = QtWidgets.QCheckBox(MainWindow)
        self.opaqueCheckbox.setGeometry(QtCore.QRect(100, 0, 70, 31))
        self.opaqueCheckbox.setObjectName("opaqueCheckbox")
        self.alwaysOnTopCheckbox = QtWidgets.QCheckBox(MainWindow)
        self.alwaysOnTopCheckbox.setGeometry(QtCore.QRect(170, 0, 91, 31))
        self.alwaysOnTopCheckbox.setObjectName("alwaysOnTopCheckbox")
        self.line = QtWidgets.QFrame(MainWindow)
        self.line.setGeometry(QtCore.QRect(76, 5, 20, 19))
        self.line.setFrameShape(QtWidgets.QFrame.VLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.frame.raise_()
        self.frame_2.raise_()
        self.buttonsBox.raise_()
        self.mouseSensitivitySlider.raise_()
        self.mouseSensitivityLabel.raise_()
        self.mouseControlCheckbox.raise_()
        self.configurationLabel.raise_()
        self.forceFeedbackCheckbox.raise_()
        self.forceFeedbackSlider.raise_()
        self.forceFeedbackLabel.raise_()
        #self.returnMouseSensitivityWhenDisabledCheckbox.raise_()
        self.opaqueCheckbox.raise_()
        self.alwaysOnTopCheckbox.raise_()
        self.line.raise_()

        self.retranslateUi(MainWindow)
        self.bind(MainWindow)

    def bind(self, MainWindow):
        self.opaqueCheckbox.clicked['bool'].connect(self.changeWindowOpacity)
        self.alwaysOnTopCheckbox.clicked['bool'].connect(self.changeAlwaysOnTopFlag)
        self.forceFeedbackCheckbox.clicked['bool'].connect(self.components.Servo.invertServoControlFlag)
        self.mouseControlCheckbox.clicked['bool'].connect(self.components.Mouse.invertMouseControlIFlag)
        #self.returnMouseSensitivityWhenDisabledCheckbox.clicked['bool'].connect(self.returnMouseSensitivityWhenDisabledCheckbox.setChecked)
        self.mouseSensitivitySlider.valueChanged['int'].connect(self.changeMouseSensitivity)
        self.forceFeedbackSlider.valueChanged['int'].connect(self.changeForceFactor)
        self.buttonsBox.button(QtWidgets.QDialogButtonBox.Discard).clicked.connect(self.startStop)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "ShapeAir"))
        self.mouseSensitivityLabel.setText(_translate("MainWindow", "Mouse sensitivity"))
        self.mouseControlCheckbox.setText(_translate("MainWindow", "Mouse control"))
        self.configurationLabel.setText(_translate("MainWindow", "Configuration"))
        self.forceFeedbackCheckbox.setText(_translate("MainWindow", "Force feedback"))
        self.forceFeedbackLabel.setText(_translate("MainWindow", "Force feedback strength"))
        #self.returnMouseSensitivityWhenDisabledCheckbox.setText(_translate("MainWindow", "Return to default when disabled"))
        self.opaqueCheckbox.setText(_translate("MainWindow", "Opaque"))
        self.alwaysOnTopCheckbox.setText(_translate("MainWindow", "Always on top"))

    def changeWindowOpacity(self):
        Ui_MainWindow.WINDOW_OPACITY = not Ui_MainWindow.WINDOW_OPACITY
        if Ui_MainWindow.WINDOW_OPACITY:
            self.MainWindow.setWindowOpacity(Ui_MainWindow.WINDOW_OPACITY_LEVEL)
        else:
            self.MainWindow.setWindowOpacity(1.0)

    def changeAlwaysOnTopFlag(self):
        Ui_MainWindow.ALWAYS_ON_TOP = not Ui_MainWindow.ALWAYS_ON_TOP
        if Ui_MainWindow.ALWAYS_ON_TOP:
            self.MainWindow.setWindowFlags(self.MainWindow.windowFlags() | QtCore.Qt.WindowStaysOnTopHint)
            self.MainWindow.show()
        else:
            self.MainWindow.setWindowFlags(self.MainWindow.windowFlags() ^ QtCore.Qt.WindowStaysOnTopHint)
            self.MainWindow.show()

    def changeMouseSensitivity(self):
        self.components.Mouse.changeMouseSensitivity(self.mouseSensitivitySlider.value())

    def changeForceFactor(self):
        self.components.Servo.changeForceFactor(self.forceFeedbackSlider.value())

    def startStop(self):
        if Ui_MainWindow.SHAPEAIR_ENABLED:
            self.buttonsBox.button(QtWidgets.QDialogButtonBox.Discard).setText('Start')
            self.components.stop()
            Ui_MainWindow.SHAPEAIR_ENABLED = False
        else:
            self.buttonsBox.button(QtWidgets.QDialogButtonBox.Discard).setText('Stop')
            self.components.start()
            Ui_MainWindow.SHAPEAIR_ENABLED = True

    def invertShapeAirEnabledFlag(self):
        Ui_MainWindow.SHAPEAIR_ENABLED = not Ui_MainWindow.SHAPEAIR_ENABLED