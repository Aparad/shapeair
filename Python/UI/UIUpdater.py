from Utils.Singleton import Singleton
from Config.Configuration import Configuration
from PyQt5 import QtWidgets


class UIUpdater(metaclass=Singleton):
    """ Helper class for updating UI elements. """
    GUI_ENABLED = None
    GUI = None
    SHAPE_AIR_LOGIC = None

    def __init__(self, ShapeAirLogic = None, GUI = None):
        UIUpdater.SHAPE_AIR_LOGIC = ShapeAirLogic
        UIUpdater.GUI = GUI
        UIUpdater.GUI_ENABLED = Configuration.read().getboolean("conf", "guienabled")

    @classmethod
    def updateGUI(cls):
        if UIUpdater.GUI_ENABLED:
            UIUpdater.GUI.MainWindow.mouseControlCheckbox.setChecked(UIUpdater.SHAPE_AIR_LOGIC.Mouse.IsMouseEnabled)
            UIUpdater.GUI.MainWindow.buttonsBox.button(QtWidgets.QDialogButtonBox.Discard).setText('Stop' if UIUpdater.SHAPE_AIR_LOGIC.IS_WORKING else 'Start')
            UIUpdater.GUI.MainWindow.invertShapeAirEnabledFlag()
