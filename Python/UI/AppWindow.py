from PyQt5 import QtWidgets
from UI.ShapeAirUI import Ui_MainWindow
from UI.UIUpdater import UIUpdater
from Utils.Singleton import Singleton


class AppWindow(QtWidgets.QMainWindow):
    """ Application window class. """
    def __init__(self, components):
        super(AppWindow, self).__init__()
        self.MainWindow = Ui_MainWindow(components)
        self.MainWindow.setupUi(self)

    def closeEvent(self, event):
        self.MainWindow.shapeAir.ShapeAirLogic.exit()
        event.accept()


class GUI(metaclass=Singleton):
    """ GUI container. """
    def __init__(self, shapeAir):
        self.appInstance = QtWidgets.QApplication([])
        self.appWindow = AppWindow(shapeAir)
        self.GUIUpdater = UIUpdater(shapeAir.ShapeAirLogic, self.appWindow)
        self.appWindow.show()
        self.appInstance.exec()
