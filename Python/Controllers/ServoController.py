from Interfaces.BlenderShapeAirInterface import BlenderShapeAirInterface
from ShapeAir.ShapeAirModule import ShapeAirModule, cancellable
from ShapeAir.ShapeAirThread import getLock, getRLock
from Utils.Singleton import Singleton
from Blender.BlenderServer import BlenderServer
from Connectors.ServoConnector import ServoConnector
from Config.Configuration import Configuration
from Tests.TestMock import TestMock
from Utils.Logger import getLogger
from time import sleep


class ServoController(BlenderShapeAirInterface, ShapeAirModule, metaclass=Singleton):
    """ Class that allows steering the Servo Motor connected to the Arduino. """
    def __init__(self):
        super(ServoController, self).__init__()
        # Enables / disables engine control
        self.ServoEnabled = False
        # Firmata board controller object
        self.Board = ServoConnector() if Configuration.read().getboolean("conf", "boardenabled") else TestMock()
        # Possible DC Engine steering value range
        self.INPUT_MIN = 0.0
        self.INPUT_MAX = 1.0
        # Servo initial position
        self._position = 0.0
        # Force feedback power factor
        self.FeedbackFactor = 1.0
        # Thread lock object
        self.Lock, self.RLock = getLock(), getRLock()
        # Set up module thread workers
        self.addWorker(self.updateServoPosition)
        # Logger
        self.Logger = getLogger()

    def setPosition(self, _position):
        if self.ServoEnabled:
            with self.RLock:
                self.POSITION = self.processInputFromBlenderIntoServoPosition(_position)
                self.Board.setServoPosition(self.POSITION)

    def processInputFromBlenderIntoServoPosition(self, inputPosition):
        self.Logger.debug("Processing Blender output: %s" % inputPosition)
        returnPosition = abs(float(inputPosition)) * self.INPUT_MAX
        self.Logger.debug("Processed into: %f" % returnPosition)
        return returnPosition

    @cancellable
    def updateServoPosition(self):
        with self.Lock:
            if BlenderServer.DATA_STATUS == BlenderServer.DATA_CORRECT:
                self.Logger.debug("Updating servo position now.")
                self.setPosition(BlenderServer.RECEIVED_DATA)
        sleep(0.25)

    def changeForceFactor(self, factor):
        with self.RLock:
            self.FeedbackFactor = 1.0 - (1.0 - factor/5)
            self.Logger.debug("Servo Motor speed factor changed to {}".format(self.FeedbackFactor))

    @property
    def POSITION(self):
        return self._position

    @POSITION.setter
    def POSITION(self, position):
        if position < self.INPUT_MIN:
            self.Logger.error("Servo steering value should be >= %d. Adjusting now." % self.INPUT_MIN)
            self._position = self.INPUT_MIN
        elif position > self.INPUT_MAX:
            self.Logger.error("Servo steering value should be <= %d. Trimming now." % self.INPUT_MAX)
            self._position = self.INPUT_MAX
        else:
            self._position = position
        self.adjustPositionByFactor(self.FeedbackFactor)

    def adjustPositionByFactor(self, factor):
        self._position = self._position * factor

    def invertServoControlFlag(self):
        with self.Lock:
            self.ServoEnabled = not self.ServoEnabled
            self.Logger.debug("Motor control changed")
