import pyautogui
from pynput.mouse import Button, Controller
from Utils.WinNotification import WinNotification
import time
from Utils.Singleton import Singleton
import ctypes
from math import sin, cos, radians, sqrt
from ShapeAir.ShapeAirModule import ShapeAirModule, cancellable
from ShapeAir.ShapeAirThread import getLock, getRLock
from Connectors.MouseConnector import MouseConnector
from Config.Configuration import Configuration
from Tests.TestMock import TestMock
from Utils.Logger import getLogger
from UI.UIUpdater import UIUpdater


class MouseController(ShapeAirModule, metaclass=Singleton):
    """ Class that allows to perform computer mouse clicks and movements. """
    def __init__(self):
        super(MouseController, self).__init__()
        # Set up thread workers
        self.addWorker(self.mouseSimulation)
        # Firmata board controller object
        self.Board = MouseConnector() if Configuration.read().getboolean("conf", "boardenabled") else TestMock()
        # Enables/Disables arduino's mouse movement control
        self.IsMouseEnabled = False
        # Boolean defining if left mouse button is down
        self.IsLeftMouseButtonDown = False
        # Time of last mouse control state change
        self.MouseControlChangeTime = time.time()
        # Boolean defining if mouse control state has changed
        self.MouseControlChanged = False
        # Mouse sensitivity factor
        self.MouseSensitivity = 1.0

        # PyAutoGUI settings #
        # Minimal number of seconds to sleep between mouse moves.
        pyautogui.MINIMUM_SLEEP = 0  # Default: 0.05
        # The number of seconds to pause after EVERY public function call.
        pyautogui.PAUSE = 0  # Default: 0.1
        # Minimum duration for mouse movements
        pyautogui.MINIMUM_DURATION = 0  # Default: 0.1
        # Disable exception thrown when mouse is in upper left screen corner
        pyautogui.FAILSAFE = False  # Default: True

        # Win 10 Notifications
        self.Notifications = WinNotification()

        # Pynput mouse controller for sending mouse button press events
        self.MouseButtonsController = Controller()

        # Current screen geometry
        self.SCREEN_WIDTH = ctypes.windll.user32.GetSystemMetrics(0)
        self.SCREEN_HEIGHT = ctypes.windll.user32.GetSystemMetrics(1)

        # Thread lock object
        self.Lock, self.RLock = getLock(), getRLock()
        # Messages logger
        self.Logger = getLogger()

        """ Empirical values for position calculations. """
        # Constant of real arm length
        self.ARM_LENGTH = 10
        self.MAX_HEIGHT = 20
        self.Y_MAX = 0.98
        self.Y_MIN = 0.1
        self.X_MAX = 0.9
        self.X_MIN = 0.1
        self.Z_MAX = 1
        self.Z_MIN = 0
        self.ANGLE_MAX = 180
        self.ANGLE_MIN = 0
        self.range_Z = self.Z_MAX - self.Z_MIN   #'Practical' range of Z potentiometer
        self.range_Y = self.Y_MAX - self.Y_MIN   #'Practical' range of Y potentiometer

    @cancellable
    def mouseSimulation(self):
        self.updateCursorPosition()

    def updateCursorPosition(self):
        """ Move computer cursor to X and Y coordinates. """
        if self.IsMouseEnabled:
            self.Logger.debug("Mouse movement.")
            pyautogui.moveTo(self.X, self.Y)
        else:
            self.Logger.debug("Mouse movement disabled")
        time.sleep(0.01)

    def invertMouseControl(self):
        """ Sets mouse control to opposite of current state.
            It is possible only if MOUSE_CONTROL_CHANGED is False (3 seconds has passed since previous change),
            to prevent too often changes due to receiving multiple keyboard input events from one 'real' keypress.
            Shows a Win10 notification about current state, after the change.
        """

        # Check if it is possible to change mouse control state
        # Count time from last change of state
        __timeFromChange = time.time() - self.MouseControlChangeTime
        self.Logger.debug("Time from last change: %s" % __timeFromChange)
        # Allow to turn mouse control back on after 3 seconds
        if self.MouseControlChanged and __timeFromChange > 3:
            self.MouseControlChanged = False

        # Change mouse control state if possible
        if self.MouseControlChanged is False:
            self.invertMouseControlIFlag()
            self.MouseControlChanged = True
            self.MouseControlChangeTime = time.time()
            self.Notifications.showWinNotification("Arduino mouse control",
                                    "Enabled" if self.IsMouseEnabled else "Disabled")
            UIUpdater().updateGUI()


    def invertMouseControlIFlag(self):
        with self.RLock:
            self.IsMouseEnabled = not self.IsMouseEnabled
            self.Logger.debug("Mouse control changed to {}".format(self.IsMouseEnabled))

    def changeMouseSensitivity(self, sensitivity):
        with self.RLock:
            self.MouseSensitivity = 1.0 - (1.0 - sensitivity/99)
            self.Logger.debug("Mouse sensitivity changed to {}".format(self.MouseSensitivity))

    def pressLeftMouseButton(self):
        if not self.IsLeftMouseButtonDown:
            self.MouseButtonsController.press(Button.left)
            self.IsLeftMouseButtonDown = True

    # Return to neutral state (release all pressed keys)
    def releaseKeyPress(self):
        if self.IsLeftMouseButtonDown:
            self.IsLeftMouseButtonDown = False
            self.MouseButtonsController.release(Button.left)

    def calculateYPosition(self):
        """ Calculates vertical position of manipulator.
            Both potentiometers are considered. """
        with self.Lock:
            Y = self.Board.getY()
            Z = self.Board.getZ()

        mapped_Z = self.Z_MAX - ((Z - self.Z_MIN) / self.range_Z)    # Mapping potentiometer values from full range to 'practical' range.
        mapped_Y = (Y - self.Y_MIN) / self.range_Y  # Mapping potentiometer values from full range to 'practical' range.
        Z_radians = radians(270 - (mapped_Z * self.ANGLE_MAX * 1.5))  # Angle at Z potentiometer in radians.
        Y_radians = radians(180 - (mapped_Y * self.ANGLE_MAX))  # Angle at Y potentiometer in radians.
        long_tri_side = sqrt(2 * (self.ARM_LENGTH*self.ARM_LENGTH) * (1 - cos(Z_radians)))  # Length of base side in a triangle with 2 equal sides (let's call it T).
        Y_angle = 180 - (mapped_Y * self.ANGLE_MAX)  # Angle between potentiometer 'Y' and ground.
        Z_angle = 270 - (mapped_Z * self.ANGLE_MAX * 1.5)  # Angle represented by potentiometer 'Z' - opposite to base side of T.
        delta = radians((180 - (180 - (mapped_Z * self.ANGLE_MAX)))/2)  # Value of one out of two equal angles in T.

        """ Depth of manipulator. 
            Determines if manipulator is in front of Y potentiometer or behind it. """
        x1 = (sin(Y_angle), cos(Y_angle))
        x1_rotated = ((x1[0] * cos(180 - Y_angle) + (x1[1] * sin(180 - Y_angle))), (-x1[0] * sin(180 - Y_angle) + (x1[1] * cos(180 - Y_angle))))
        x2 = (x1_rotated[0] + sin(Z_angle), x1_rotated[1] + cos(Z_angle))
        x2_rotated_back = ((x2[0] * cos(-(180 - Y_angle)) + (x2[1] * sin(-(180 - Y_angle)))), (-x2[0] * sin(-(180 - Y_angle)) + (x2[1] * cos(-(180 - Y_angle)))))
        is_position_left = x2_rotated_back[0] < 0

        """ Heigth of manipulator. 
            If manipulator is in front of 'Y axis' different angles are used for calculations. """
        if is_position_left:
            h = (long_tri_side * sin(Y_radians - delta))
        else:
            h = (long_tri_side * sin((radians(180) - Y_radians) + delta))

        self.Logger.debug("Z: %s" % str(Z))
        self.Logger.debug("Y: %s" % str(Y))
        self.Logger.debug("Z angle: %s" % str(Z_angle))
        self.Logger.debug("Y angle: %s" % str(Y_angle))
        self.Logger.debug("delta: %s" % str(delta))
        self.Logger.debug("side length: %s" % str(long_tri_side))
        self.Logger.debug("h: %s" % str(h))
        self.Logger.debug("Y: %s" % str(1 - (h / self.MAX_HEIGHT)))

        return 1 - (h / self.MAX_HEIGHT)  # Inverting and mapping the vertical position to range [0, 1].

    def calculateXPosition(self):
        with self.Lock:
            _X = self.Board.getX()
        self.Logger.debug("X: %s" % str(1 - _X))
        return (1.25*_X * self.X_MAX) + self.X_MIN

    @property
    def X(self):
        return int(self.calculateXPosition() * self.SCREEN_WIDTH * self.MouseSensitivity)

    @property
    def Y(self):
        return int(self.calculateYPosition() * self.SCREEN_HEIGHT * self.MouseSensitivity)

    @property
    def Z(self):
        return self.Board.getZ()

    # Variables for testing purposes.
    @property
    def _Local_X(self):
        return pyautogui.position()[0]

    @property
    def _Local_Y(self):
        return pyautogui.position()[1]
