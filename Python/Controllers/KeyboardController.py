from Utils.Singleton import Singleton
from time import sleep
from ShapeAir.ShapeAirModule import ShapeAirModule, cancellable
from ShapeAir.ShapeAirThread import getLock
from Connectors.KeyboardConnector import KeyboardConnector
from Config.Configuration import Configuration
from Tests.TestMock import TestMock
from Utils.Logger import getLogger
from Controllers.MouseController import MouseController


class KeyboardController(ShapeAirModule, metaclass=Singleton):
    """ Class that manages executing actions according to button presses on Arduino keyboard. """
    def __init__(self):
        super(KeyboardController, self).__init__()
        # Bind key numbers with actions
        self.ButtonNumberToFunction = {
            -1: MouseController().releaseKeyPress,
            3: MouseController().pressLeftMouseButton,
            4: MouseController().invertMouseControl
        }
        # TODO: Button for MMB/RMB
        # TODO: Button for changing sculpting mode
        # TODO: Button for ctrl+z
        # Firmata board controller object
        self.Board = KeyboardConnector() if Configuration.read().getboolean("conf", "boardenabled") else TestMock()
        # Data variables
        self.CurrentKeyValue = 0
        # Add module thread workers
        self.addWorker(self.keyboardSimulation)
        # Messages logger
        self.Logger = getLogger()
        # Thread lock
        self.Lock = getLock()

    @cancellable
    def keyboardSimulation(self):
        self.executeActionForCurrentButton()

    def executeActionForCurrentButton(self):
        """ Executes function from dictionary supplied at creation according to the number
            of currently pressed button on Arduino keyboard. """
        self.Logger.debug("Keyboard simulation active now.")
        try:
            with self.Lock:
                self.CurrentKeyValue = self.Board.getKeyNumber()
                self.ButtonNumberToFunction[self.CurrentKeyValue]()
            self.Logger.debug("Pressed key: %s" % self.CurrentKeyValue)
        except KeyError:
            self.Logger.error("Button #%d has no function bound" % self.CurrentKeyValue)
        sleep(0.01)
